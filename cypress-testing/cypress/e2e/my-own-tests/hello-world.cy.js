/// <reference types="cypress" />

describe("Basic Desktop Tests", () => {
  beforeEach(() => {
    cy.viewport("macbook-16");
    cy.visit("https://codedamn.com");
  });

  it("Login page looks good", () => {
    cy.contains("Sign in").click();
    cy.contains("Sign in to codedamn").should("exist");
    cy.contains("Forgot your password?").should("exist");
    cy.contains("Sign in").should("exist");
    cy.contains("Create one").should("exist");
  });

  it("The login page links work", () => {
    // 1. Sign in page
    cy.contains("Sign in").click();
    // 2. Password reset page
    cy.contains("Forgot your password?").click({ force: true });
    // 3. Verify your page url
    cy.url().should("include", "/password-reset");
    // 4. go back, on the sign in page
    cy.go("back");

    cy.contains("Create one").click({ force: true });
    cy.url().should("include", "register");
    cy.go("back");
  });

  it("Login should display correct error", () => {
    cy.contains("Sign in").click();
    cy.contains("Unable to authorize.").should("not.exist");

    cy.get('[data-testid="username"]').type("hello", { force: true });
    cy.get('[data-testid="username"]').type("hello", { force: true });
    cy.get('[data-testid="password"]').type("admin", { force: true });

    cy.get('[data-testid="login"]').click({ force: true });
    cy.contains("Unable to authorize.").should("exist");
  });

  it("Login should work fine", () => {
    cy.contains("Sign in").click();

    cy.get('[data-testid="username"]').type("indra.kusumaadi7@gmail.com", {
      force: true,
    });
    cy.get('[data-testid="username"]').type("indra.kusumaadi7@gmail.com", {
      force: true,
    });
    cy.get('[data-testid="password"]').type("Indra123!@#=", { force: true });

    cy.get('[data-testid="login"]').click({ force: true });
    cy.url().should("include", "/dashboard");
  });
});
