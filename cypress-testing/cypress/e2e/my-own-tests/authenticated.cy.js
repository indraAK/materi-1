/// <reference types="cypress" />
const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9";

describe("Basic Authenticated Desktop Tests", () => {
  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.then(() => {
      window.localStorage.setItem("__auth__token", token);
    });
  });

  beforeEach(() => {
    cy.viewport(1280, 720);
    cy.visit("https://codedamn.com");
  });

  it("Should load playground correctly", () => {
    cy.visit("https://codedamn.com/playground/html");
    cy.log("Checking for sidabar");
    cy.contains("Trying to connect").should("exist");
    cy.log("Checking bottom left button");
    cy.get("[data-testid=xterm-controls] > div").should(
      "contain.text",
      "Connecting"
    );
    cy.get("Trying to establish connecting").should("exist");
    cy.log("Playground is initializing");
    cy.get("Setting up the challenge").should("not.exist");
  });
});
